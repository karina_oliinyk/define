# DEFINE

There are five files in folder src:

* define.js: contain module with the function define and realisation of this function
* index.js: import function sum
* sum.js: define function sum and export it
* dir.js: define function dir and export it and import function sum
* result.js: define function result and export it and import functions sum and dir

## Objective of the project:

* understand the module
* understand how the define to work
* implement own realisation of the define
